<?php
	//INI. Datos de la transacción
	$monto = "25000"; //OJO en pesos chilenos sin decimales
	//Agregar ID a la session
	
	$transaccionId = "111222333";
	//FIN. Datos de la transacción
	
	//INI. Fijo ojo esto no debe cambiar
	
	//Comercio Produccion
	$codigoComercio = "0237797";
	//Comercio Prueba
	$codigoComercio = "237845";
	
	$nombreComercio = "Regala una Plaza";

	$nombreDonacion = "Donacion";
	$servidor = $_SERVER["SERVER_NAME"];
	$urlAceptado = $servidor."/transactions/aceptado.php";
	$urlRechazo = $servidor."/transactions/rechazado.php";
	$urlPendiente = $servidor."/transactions/pending.php";
	//Medios de pago: Dinero Mail, TC, Tarjetas casa comercial
	$medioDePago = "cl_dm;cl_visa,1,3,6;cl_master,1,3,6;cl_diners,1,3,6;cl_amex,1,3,6;cl_ripley;cl_presto";
	//Medios de pago: Dinero Mail, TC
	//$medioDePago = "cl_dm;cl_visa,1,3,6;cl_master,1,3,6;cl_diners,1,3,6;cl_amex,1,3,6";
	
	//FIN. Fijo ojo esto no debe cambiar
	
	//INI. Datos que podemos precargar si los tenemos
	$nombreCliente = "";
	$apellidoCliente = "";
	$sexoCliente = "";
	$rutCliente = "";
	$emailCliente = "";
	$telefonoCliente = "";
	$direccionCalleCliente = "";
	$direccionNumeroCliente = "";
	//FIN. Datos que podemos precargar si los tenemos
	 
	// Necesario cada vez que se lean 
	// o guarden variables de sesión. 
	$_SESSION['txid']=$transaccionId;
?>
<form id="donar_con_dineromail" action="https://checkout.dineromail.com/CheckOut" method="post" style="display:none;"> 
	<!-- Sale settings --> 
	<input type="hidden" name="tool" value="button" /> 
	<input type="hidden" name="merchant" value=<?php echo("'".$codigoComercio."'")?>/> 
	<input type="hidden" name="country_id" value="3" />
	<input type="hidden" name="languge" value="es" /> 
	<input type="hidden" name="seller_name" value=<?php echo("'".$nombreComercio."'")?> /> 
	<input type="hidden" name="transaction_id" value=<?php echo("'".$transaccionId."'")?>  /> 
	<input type="hidden" name="currency" value="clp" /> 
	<input type="hidden" name="ok_url" value=<?php echo("'".$urlAceptado."'")?> /> 
	<input type="hidden" name="error_url" value=<?php echo ("'".$urlRechazo."'") ?> /> 
	<input type="hidden" name="pending_url" value=<?php echo ("'".$urlPendiente."'") ?> /> 
	<input type="hidden" name="additional_fixed_charge" value="0" /> 
	<input type="hidden" name="additional_fixed_charge_currency" value="clp" /> 
	<input type="hidden" name="buyer_message" value="0" /> 
	<input type="hidden" name="header_image" value="https://static.e-junkie.com/sslpic/30174.c49d331ff787c87b1dafad4aceaeac83.jpg" /> 
	<input type="hidden" name="header_width" value="1" /> 
	<input type="hidden" name="change_quantity" value="0" /> 
	<input type="hidden" name="display_shipping" value="0" /> 
	<input type="hidden" name="display_additional_charge" value="0" /> 
	<input type="hidden" name="expanded_step_ PM" value="0" /> 
	<input type="hidden" name="expanded_step_ AD" value="0" /> 
	<input type="hidden" name="expanded_step_ SC" value="0" /> 
	<input type="hidden" name="expanded_sale_detail" value="0" /> 
	<!-- Paymet Method --> 
	<input type="hidden" name="payment_method_available" value=<?php echo ("'".$medioDePago."'") ?> /> 
	<input type="hidden" name="payment_method_1" value="cl_visa" /> 
	<!-- Items --> 
	<!-- Item 1 --> 
	<input type="hidden" name="item_name_1" value=<?php echo ("'".$nombreDonacion."'") ?> /> 
	<input type="hidden" name="item_code_1" value="001" /> 
	<input type="hidden" name="item_quantity_1" value="1" /> 
	<input type="hidden" name="item_ammount_1" value=<?php echo("'".$monto.".00'")?> /> 
	<input type="hidden" name="item_currency_1" value="clp" /> 
	<input type="hidden" name="shipping_type_1" value="0" /> 
	<input type="hidden" name="weight_1" value="kg" /> 
	<input type="hidden" name="item_weight_1" value="0000" /> 
	<input type="hidden" name="shipping_currency_1" value="chl" /> 
	<!-- Buyer info --> 
	<input type="hidden" name="buyer_name" value=<?php echo("'".$nombreCliente."'")?> /> 
	<input type="hidden" name="buyer_lastname" value=<?php echo("'".$apellidoCliente."'") ?> /> 
	<input type="hidden" name="buyer_sex" value=<?php echo ("'".$sexoCliente."'") ?> /> 
	<input type="hidden" name="buyer_nacionality" value="chl" /> 
	<input type="hidden" name="buyer_document_type" value="dni" /> 
	<input type="hidden" name="buyer_document_number" value=<?php echo ("'".$rutCliente."'") ?> /> 
	<input type="hidden" name="buyer_email" value=<?php echo ("'".$emailCliente."'") ?> /> 
	<input type="hidden" name="buyer_phone" value=<?php echo ("'".$telefonoCliente."'") ?> />  
	<input type="hidden" name="buyer_phone_extension" value="" /> 
	<input type="hidden" name="buyer_zip_code" value="8320000" /> 
	<input type="hidden" name="buyer_street" value=<?php echo ("'".$direccionCalleCliente."'") ?> /> 
	<input type="hidden" name="buyer_number" value=<?php echo ("'".$direccionNumeroCliente."'") ?> /> 
	<input type="hidden" name="buyer_complement" value="" /> 
	<input type="hidden" name="buyer_city" value="" /> 
	<input type="hidden" name="buyer_state" value="" /> 
	<input type="hidden" name="buyer_country" value="chl" /> 
	<!-- Color Design --> 
	<input type="hidden" name="step_color" value="E7E7E7" /> 
	<input type="hidden" name="hover_step_color" value="DFEFFF" /> 
	<input type="hidden" name="links_color" value="0000D4" /> 
	<input type="hidden" name="font_color" value="6699CC" /> 
	<input type="hidden" name="border_color" value="F99800" /> 
	<input type="hidden" name="button_color" value="6699CC" /> 
	<!-- Additional data --> 
	<!-- Item 1 --> 
	<!--input type="hidden" name="additional_var_description_1" value="XXXXXXX" /> 
	<input type="hidden" name="additional_var_value_1" value="XXXXXXX" /--> 
	<input type="hidden" name="additional_var_visible_1" value="0" /> 
	<input type="hidden" name="additional_var_required_1" value="0" /> 
	<input id="Submit" type="submit" value="button"/> 
</form>