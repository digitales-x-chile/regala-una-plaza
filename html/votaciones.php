<?php
require_once "database/config.php";
$results = mysql_query("SELECT * FROM `schools`") or trigger_error(mysql_error());
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Regala una plaza</title>

	<link rel="stylesheet" href="assets/css/public/reset.css" type="text/css" />
	<link rel="stylesheet" href="assets/css/main.css" type="text/css" />
	<link rel="stylesheet" href="assets/js/public/jquery.jcarousel/jquey.jcarousel.skin.css" type="text/css" />
	<link rel="stylesheet" href="assets/js/public/facebox/facebox.css" type="text/css" />
	<style type="text/css" media="screen">
	#plaza_container{
		height: 150px;
	}
		#plaza_container_left{
			width: 623px;
			height: 91px;
			text-align: left;
			margin-top: 40px;
			font-size: 13px;
			line-height: 18px;
		}
		#plaza_container_left h2{
			margin-bottom: 20px;
			font-size: 20px;
			color: #666;
		}
		#donar{
			top: 39px;
		}
		#votacion_container{
			height: auto;
		}
		#votacion_container{
			overflow: auto;
		}
		#votacion_container ul li{
			height: 261px;
			width: 248px;
			background: url(/assets/images/caja_votacion.png) no-repeat;
			margin: 0px 0px 20px 40px;
			float: left;
			color: #FFF;
			padding: 15px 10px;
		}
		#votacion_container ul li h3{
			font-size: 15px;
			font-weight: bold;
			margin-bottom: 6px;
		}
		#votacion_container ul li h4{
			font-size: 13px;
			margin-bottom: 15px;
		}
		.btn_votar img{
			margin: 6px 0 0 130px;
		}
	</style>
	<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if lte IE 7]>
		<script src="js/IE8.js" type="text/javascript"></script><![endif]-->
	<!--[if lt IE 7]>
		<link rel="stylesheet" type="text/css" media="all" href="css/ie6.css"/>
	<![endif]-->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
	<script type="text/javascript" src="/assets/js/public/jquery.jcarousel/jquery.jcarousel.min.js"></script>
	<script type="text/javascript" src="/assets/js/pages/index.js"></script>
	<script type="text/javascript" src="/assets/js/public/facebox/facebox.js"></script>
</head>
	<body>
		<div class="full header1">
			<div class="wrap">
				<img id="logo1" src="/assets/images/logo_charitybox.png" alt="Logo1"/>
				<!--ul id="header1_menu">
					<li><a href="#">Elige un sueño</a></li>
					<li><a href="#">Regala una plaza</a></li>
					<li><a href="#">Difunde</a></li>
					<li><a href="#">Prensa</a></li>
				</ul-->
				<img id="logo2" src="/assets/images/fundacionmustaki.png" alt="Logo2"/>
			</div>
		</div>
		<div class="full header2">
			<div class="wrap">
				<div class="header2_1" class="font">
					Con las ideas de los niños, construiremos la mejor plaza de juegos. Necesitamos tu donación, para hacer real su sueño.
				</div>
				<div class="header2_2">
					<ul>
						<li><img src="/assets/images/paso1.png" alt=""></li>
						<li><img src="/assets/images/paso2.png" alt=""></li>
						<li><img src="/assets/images/paso3.png" alt=""></li>
					</ul>
				</div>
			</div>
		</div>
		<div id="main_content" class="wrap">
			<!--div id="syndicate">
				<div><a href="http://twitter.com/share" class="twitter-share-button" data-text="Ayuda a regalar una plaza" data-count="horizontal" data-via="regalaunaplaza" data-lang="es">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script></div>
				<div><iframe src="http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fexample.com%2Fpage%2Fto%2Flike&amp;layout=button_count&amp;show_faces=false&amp;width=450&amp;action=recommend&amp;colorscheme=light&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:21px;" allowTransparency="true"></iframe></div>
			</div-->
			<div id="plaza_container">
				<div id="plaza_container_left">
					<h2>Vota por tu plaza favorita</h2>
					<div>
						<p>Entre junio y agosto del año 2010, fueron cientos los colegios que participaron en el concurso de construcción con piezas de Lego para realizar <b>"Mi mejor plaza de juegos"</b>.</p>
						<p>Esta es la última y más importante etapa del proyecto. Es aquí donde todos tenemos la oportunidad de hacer realidad el sueño de los niños. Poder construir a escala real en 1.000m&sup2; el proyecto ganada de "mi mejor plaza de juegos"</p>
					</div>
					<img id="donar" src="/assets/images/btn_donar.png" alt=""/>
				</div>
			</div>

			<div id="votacion_container">
				<ul>
					<?php
						while($row = mysql_fetch_array($results)){ 
							echo "<li>";
								echo "<h3>$row[name]</h3>";
								echo "<h4>$row[location]</h4>";
								echo "<div><div class='clipwrapper_votacion'><div class='clip_votacion'><img src='$row[image]' alt=''/></div></div></div>";
								echo "<a href='javascript:votar($row[id])' class='btn_votar'><img src='/assets/images/btn_votar_plaza.png' alt='Votar'></a>";
							echo "</li>";
						}
					?>
				</ul>
			</div>
		</div>
		<div class="footer">
			<div class="wrap">
				<div id="up">
					<a href="#"><img id="logo1" src="/assets/images/1.jpg" alt="Logo1"/></a>
				</div>
				<div id="es_de">Regala una plaza es un proyecto de <a href="#">Charitybox</a> en conjunto con <a href="#">Digitales x Chile</a></div>
				<ul id="footer_menu">
					<li><a href="#">Equipo</a></li>
					<li><a href="#">Contacto</a></li>
				</ul>
			</div>
		</div>
	</body>
</html>
