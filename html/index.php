<?php
	session_start(); // incio de uso de sesiones.
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Regala una plaza</title>

	<link rel="stylesheet" href="/assets/css/public/reset.css" type="text/css" />
	<link rel="stylesheet" href="/assets/css/main.css" type="text/css" />
	<link rel="stylesheet" href="/assets/js/public/jquery.jcarousel/jquey.jcarousel.skin.css" type="text/css" />
	<link rel="stylesheet" href="/assets/js/public/facebox/facebox.css" type="text/css" />
	<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if lte IE 7]>
		<script src="js/IE8.js" type="text/javascript"></script><![endif]-->
	<!--[if lt IE 7]>
		<link rel="stylesheet" type="text/css" media="all" href="css/ie6.css"/>
	<![endif]-->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
	<script type="text/javascript" src="/assets/js/public/jquery.jcarousel/jquery.jcarousel.min.js"></script>
	<script type="text/javascript" src="/assets/js/pages/index.js"></script>
	<script type="text/javascript" src="/assets/js/public/facebox/facebox.js"></script>
</head>
	<body>
		<div class="full header1">
			<div class="wrap">
				<img id="logo1" src="/assets/images/logo_charitybox.png" alt="Logo1"/>
				<!--ul id="header1_menu">
					<li><a href="#">Elige un sueño</a></li>
					<li><a href="#">Regala una plaza</a></li>
					<li><a href="#">Difunde</a></li>
					<li><a href="#">Prensa</a></li>
				</ul-->
				<img id="logo2" src="/assets/images/fundacionmustaki.png" alt="Logo2"/>
			</div>
		</div>
		<div class="full header2">
			<div class="wrap">
				<div class="header2_1" class="font">
					Con las ideas de los niños, construiremos la mejor plaza de juegos. Necesitamos tu donación, para hacer real su sueño.
				</div>
				<div class="header2_2">
					<ul>
						<li><img src="/assets/images/paso1.png" alt=""></li>
						<li><img src="/assets/images/paso2.png" alt=""></li>
						<li><img src="/assets/images/paso3.png" alt=""></li>
					</ul>
				</div>
			</div>
		</div>
		<div id="main_content" class="wrap">
			<!--div id="syndicate">
				<div><a href="http://twitter.com/share" class="twitter-share-button" data-text="Ayuda a regalar una plaza" data-count="horizontal" data-via="regalaunaplaza" data-lang="es">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script></div>
				<div><iframe src="http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fexample.com%2Fpage%2Fto%2Flike&amp;layout=button_count&amp;show_faces=false&amp;width=450&amp;action=recommend&amp;colorscheme=light&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:21px;" allowTransparency="true"></iframe></div>
			</div-->
			<div id="plaza_container">
				<img src="/assets/images/mensaje_a_donar.png" alt="">
				<img id="donar" src="/assets/images/btn_donar.png" alt="">
				<img id="metodo_pago" src="/assets/images/metodospago.png" alt="metodos de pago">
			</div>
			<div id="plaza_suenos_menu">
				<div class="title">Mira lo que ellos sueñan:</div>
				<ul id="flicker_images" class="jcarousel-skin-tango">

				</ul>
			</div>

			<div id="votacion_container">
				<h2>Estas son algunas de las plazas</h2>
				<div class="line"></div>
				<div id="votacion_container_left" class="font">
					<p>Entre junio y agosto del año 2010, fueron cientos los colegios que participaron en el concurso de construcción con piezas de Lego para realizar <b>"Mi mejor plaza de juegos"</b>.</p>
					<p>Esta es la última y más importante etapa del proyecto. Es aquí donde todos tenemos la oportunidad de hacer realidad el sueño de los niños. Poder construir a escala real en 1.000m&sup2; el proyecto ganada de "mi mejor plaza de juegos"</p>
					<div id="votar_button"><a href="#">Quiero elegir la mejor plaza de juegos</a></div>
				</div>
				<div id="votacion_container_right">
					<div class="marco"><div class="clipwrapper"><div class="clip">
						<img src="/assets/images/paso2.png" alt=""/>
					</div></div></div>
					<div class="marco"><div class="clipwrapper"><div class="clip">
						<img src="/assets/images/paso2.png" alt=""/>
					</div></div></div>
					<div class="marco"><div class="clipwrapper"><div class="clip">
						<img src="/assets/images/paso2.png" alt=""/>
					</div></div></div>
					<div class="marco"><div class="clipwrapper"><div class="clip">
						<img src="/assets/images/paso2.png" alt=""/>
					</div></div></div>
					<div class="marco"><div class="clipwrapper"><div class="clip">
						<img src="/assets/images/paso2.png" alt=""/>
					</div></div></div>
					<div class="marco"><div class="clipwrapper"><div class="clip">
						<img src="/assets/images/paso2.png" alt=""/>
					</div></div></div>
				</div>
			</div>
			<!--div id="columns3">
				<div class="column3 first box_round">hola</div>
				<div class="column3 second">hola2</div>
				<div class="column3 third">hola3</div>
			</div>
			<div class="line"></div>
			<div id="nos_ayudan">
				<div class="title">Ellos nos estan ayudando a realizar este sueño</div>
				<div class="inst_ayuda_container">
					<div class="inst_ayuda"></div>
					<div class="inst_ayuda"></div>
					<div class="inst_ayuda"></div>
					<div class="inst_ayuda"></div>
					<div class="inst_ayuda"></div>
					<div class="inst_ayuda"></div>
					<div class="inst_ayuda"></div>
					<div class="inst_ayuda"></div>
					<div class="inst_ayuda"></div>
					<div class="inst_ayuda"></div>
					<div class="inst_ayuda"></div>
					<div class="inst_ayuda"></div>
					<div class="inst_ayuda"></div>
					<div class="inst_ayuda"></div>
					<div class="inst_ayuda"></div>
					<div class="inst_ayuda"></div>
					<div class="inst_ayuda"></div>
					<div class="inst_ayuda"></div>
					<div class="inst_ayuda"></div>
					<div class="inst_ayuda"></div>
				</div>
			</div>
			<div class="line"></div-->
		</div>
		<div class="footer">
			<div class="wrap">
				<div id="up">
					<a href="#"><img id="logo1" src="/assets/images/1.jpg" alt="Logo1"/></a>
				</div>
				<div id="es_de">Regala una plaza es un proyecto de <a href="#">Charitybox</a> en conjunto con <a href="#">Digitales x Chile</a></div>
				<ul id="footer_menu">
					<li><a href="#">Equipo</a></li>
					<li><a href="#">Contacto</a></li>
				</ul>
			</div>
		</div>
		<form id="formadd" style="display:none">
			<label>
				Nombres <span class="detail red">( requerido )</span>
				<input type="text" name="first_name" id ="first_name" class="input required" />
			</label>
			<label>
				Apellidos <span class="detail red">( requerido )</span>
				<input type="text" name="last_name" id ="last_name" class="input required" />
			</label>
			<label>
				RUT <span class="detail red">( requerido )</span>
				<input type="text" name="rut" id ="rut" class="input required rut" />
			</label>
			<label>
				Email <span class="detail red">( requerido )</span>
				<input type="text" name="email" id ="email" class="input required email" />
			</label>
			<label>
				Twitter <span class="detail">( Solo tu nick )</span>
				<input type="text" name="twitter" id="twitter" class="input twitter" />
			</label>
			<label>
				Quiero donar porque				<textarea id="message" name="message"></textarea>
				<div id="message-info">Te quedan 100 caracteres disponibles.</div>
			</label>

			<!--label>
				Facebook <span class="detail">( Solo tu usuario)</span>
				<input type="text" name="facebook" id ="facebook" class="input facebook" />
			</label>
			<label>
				Sitio Web <span class="detail">( debes incluir http:// )</span>
				<input type="text" name="url" id ="url" class="input url" />
			</label>
			<input type="hidden" value="0" name="donator" id="donator" />
			<input type="hidden" name="fb" id="fb" value="0"-->
			<div class="center">
				<input id="enviar" type="button" value="Continuar" class="submit" onclick="adduser()"/>
			</div>
		</form>
		<!--form id="formvote" style="display:block">
			<input type="hidden" name="school_id" value="1" />
			<label>
				Email <span class="detail red">( requerido )</span>
				<input type="text" name="email" id ="email" class="input required" />
			</label>
			<div class="center">
				<input id="enviar" type="button" value="Continuar" class="submit" onclick="addvote()"/>
			</div>
		</form-->

		<?php include 'utils/pay_form.php';?>

	</body>
</html>
