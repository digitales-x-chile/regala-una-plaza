DROP TABLE IF EXISTS `schools`;
CREATE TABLE `schools` (
  `id` int(11) NOT NULL auto_increment,
  `school` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) TYPE=innodb default charset=utf8;


DROP TABLE IF EXISTS `votes`;
CREATE TABLE `votes` (
  `id` int(11) NOT NULL auto_increment,
  `school_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `session` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) TYPE=innodb default charset=utf8;