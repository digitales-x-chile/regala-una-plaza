create table users (
	id int unsigned primary key auto_increment
	,first_name text not null
	,last_name text not null
	,rut text not null
	,email text not null
	,twitter text not null
	,message text not null
) TYPE=innodb default charset=utf8;