<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title><?php echo $title_for_layout?></title>

	<link rel="stylesheet" href="/css/public/reset.css" type="text/css" />
	<link rel="stylesheet" href="/css/main.css" type="text/css" />
	<link rel="stylesheet" href="/js/public/jquery.jcarousel/jquery.jcarousel.skin.css" type="text/css" />
	<link rel="stylesheet" href="/js/public/facebox/facebox.css" type="text/css" />
	<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if lte IE 7]>
		<script src="js/IE8.js" type="text/javascript"></script><![endif]-->
	<!--[if lt IE 7]>
		<link rel="stylesheet" type="text/css" media="all" href="css/ie6.css"/>
	<![endif]-->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
	<script type="text/javascript" src="/js/public/jquery.jcarousel/jquery.jcarousel.min.js"></script>
	<script type="text/javascript" src="/js/public/facebox/facebox.js"></script>
</head>
	<body>
		<?php echo $this->element('header'); ?>
		
		<?php echo $content_for_layout ?>
		
		<?php echo $this->element('footer'); ?>

	</body>
</html>
