<script type="text/javascript" src="/js/pages/index.js"></script>
<div id="main_content" class="wrap">
	<!--div id="syndicate">
		<div><a href="http://twitter.com/share" class="twitter-share-button" data-text="Ayuda a regalar una plaza" data-count="horizontal" data-via="regalaunaplaza" data-lang="es">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script></div>
		<div><iframe src="http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fexample.com%2Fpage%2Fto%2Flike&amp;layout=button_count&amp;show_faces=false&amp;width=450&amp;action=recommend&amp;colorscheme=light&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:21px;" allowTransparency="true"></iframe></div>
	</div-->
	<div id="plaza_container">
		<img src="/img/mensaje_a_donar.png" alt="">
		<img id="donar" src="/img/btn_donar.png" alt="">
		<img id="metodo_pago" src="/img/metodospago.png" alt="metodos de pago">
	</div>
	<div id="plaza_suenos_menu">
		<div class="title">Mira lo que ellos sueñan:</div>
		<ul id="flicker_images" class="jcarousel-skin-tango">

		</ul>
	</div>

	<div id="votacion_container">
		<h2>Estas son algunas de las plazas</h2>
		<div class="line"></div>
		<div id="votacion_container_left" class="font">
			<p>Entre junio y agosto del año 2010, fueron cientos los colegios que participaron en el concurso de construcción con piezas de Lego para realizar <b>"Mi mejor plaza de juegos"</b>.</p>
			<p>Esta es la última y más importante etapa del proyecto. Es aquí donde todos tenemos la oportunidad de hacer realidad el sueño de los niños. Poder construir a escala real en 1.000m&sup2; el proyecto ganada de "mi mejor plaza de juegos"</p>
			<div id="votar_button"><a href="/votaciones">Quiero elegir la mejor plaza de juegos</a></div>
		</div>
		<div id="votacion_container_right">
			<div class="marco"><div class="clipwrapper"><div class="clip">
				<img src="/img/plazas/uno_chica.png" alt=""/>
			</div></div></div>
			<div class="marco"><div class="clipwrapper"><div class="clip">
				<img src="/img/plazas/dos_chica.png" alt=""/>
			</div></div></div>
			<div class="marco"><div class="clipwrapper"><div class="clip">
				<img src="/img/plazas/tres_chica.png" alt=""/>
			</div></div></div>
			<div class="marco"><div class="clipwrapper"><div class="clip">
				<img src="/img/plazas/cuatro_chica.png" alt=""/>
			</div></div></div>
			<div class="marco"><div class="clipwrapper"><div class="clip">
				<img src="/img/plazas/cinco_chica.png" alt=""/>
			</div></div></div>
			<div class="marco"><div class="clipwrapper"><div class="clip">
				<img src="/img/plazas/seis_chica.png" alt=""/>
			</div></div></div>
		</div>
	</div>
	<!--div id="columns3">
		<div class="column3 first box_round">hola</div>
		<div class="column3 second">hola2</div>
		<div class="column3 third">hola3</div>
	</div>
	<div class="line"></div>
	<div id="nos_ayudan">
		<div class="title">Ellos nos estan ayudando a realizar este sueño</div>
		<div class="inst_ayuda_container">
			<div class="inst_ayuda"></div>
			<div class="inst_ayuda"></div>
			<div class="inst_ayuda"></div>
			<div class="inst_ayuda"></div>
			<div class="inst_ayuda"></div>
			<div class="inst_ayuda"></div>
			<div class="inst_ayuda"></div>
			<div class="inst_ayuda"></div>
			<div class="inst_ayuda"></div>
			<div class="inst_ayuda"></div>
			<div class="inst_ayuda"></div>
			<div class="inst_ayuda"></div>
			<div class="inst_ayuda"></div>
			<div class="inst_ayuda"></div>
			<div class="inst_ayuda"></div>
			<div class="inst_ayuda"></div>
			<div class="inst_ayuda"></div>
			<div class="inst_ayuda"></div>
			<div class="inst_ayuda"></div>
			<div class="inst_ayuda"></div>
		</div>
	</div>
	<div class="line"></div-->
</div>
<form id="formadd" style="display:none">
	<label>
		Nombres <span class="detail red">( requerido )</span>
		<input type="text" name="first_name" id ="first_name" class="input required" />
	</label>
	<label>
		Apellidos <span class="detail red">( requerido )</span>
		<input type="text" name="last_name" id ="last_name" class="input required" />
	</label>
	<label>
		RUT <span class="detail red">( requerido )</span>
		<input type="text" name="rut" id ="rut" class="input required rut" />
	</label>
	<label>
		Email <span class="detail red">( requerido )</span>
		<input type="text" name="email" id ="email" class="input required email" />
	</label>
	<label>
		Twitter <span class="detail">( Solo tu nick )</span>
		<input type="text" name="twitter" id="twitter" class="input twitter" />
	</label>
	<label>
		Quiero donar porque				<textarea id="message" name="message"></textarea>
		<div id="message-info">Te quedan 100 caracteres disponibles.</div>
	</label>
	<div class="center">
		<input id="enviar" type="button" value="Continuar" class="submit" onclick="adduser()"/>
	</div>
</form>
<?php echo $this->element('dineromail'); ?>