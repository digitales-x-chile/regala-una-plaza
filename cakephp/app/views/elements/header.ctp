<div class="full header1">
	<div class="wrap">
		<img id="logo1" src="/img/logo_charitybox.png" alt="Logo1"/>
		<!--ul id="header1_menu">
			<li><a href="#">Elige un sueño</a></li>
			<li><a href="#">Regala una plaza</a></li>
			<li><a href="#">Difunde</a></li>
			<li><a href="#">Prensa</a></li>
		</ul-->
		<img id="logo2" src="/img/fundacionmustaki.png" alt="Logo2"/>
	</div>
</div>
<div class="full header2">
	<div class="wrap">
		<div class="header2_1" class="font">
			Con las ideas de los niños, construiremos la mejor plaza de juegos. Necesitamos tu donación, para hacer real su sueño.
		</div>
		<div class="header2_2">
			<ul>
				<li><img src="/img/paso1.png" alt=""></li>
				<li><img src="/img/paso2.png" alt=""></li>
				<li><img src="/img/paso3.png" alt=""></li>
			</ul>
		</div>
	</div>
</div>